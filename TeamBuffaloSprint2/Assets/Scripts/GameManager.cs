﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Canvas characterChangeCanvas;
    public List<Button> changeCharButtonList;
    public bool changeCharMenuActive;
    public GameObject changingCharacter = null;

    public int speed = 1;

    public int spendingTime = 0;
    int spendingTimeIncrease = 3;

    public int enemyLives = 3;
    public int playerLives = 3;

    public List<GameObject> playerUnits = new List<GameObject>();
    public List<GameObject> enemyUnits = new List<GameObject>();

    Transform enemySpawnPos;
    public GameObject enemyPrefab;
    int startWave = 3;
    int waitTimeMax = 6;
    int waitTimeMin = 1;

    public bool doubleTime = false;

    bool menuUp = false;
    bool pauseGame = false;
    bool qualsUp = false;
    bool spawningEnemies = true;
    public GameObject quals;

    void Start()
    {
        enemySpawnPos = GameObject.Find("EnemySpawnPoint").transform;
        StartCoroutine(AddSpendingTime());
        StartCoroutine(SpawnEnemies());
    }

    void Update()
    {
        //for playtesting & presentation 

        //bring up menu to switch char
        if (Input.GetKeyDown(KeyCode.S))
        {
            for(int i = 0; i < changeCharButtonList.Count; i++)
            {
                changeCharButtonList[i].GetComponent<CharChangeButtonClass>().player = playerUnits[0];
            }
            print("switch");
            if (!menuUp)
            {
                characterChangeCanvas.gameObject.SetActive(true);
                menuUp = true;
            }
            else
            {
                characterChangeCanvas.gameObject.SetActive(false);
                menuUp = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            print("pause");
            if (!pauseGame)
            {
                Time.timeScale = 0;
                pauseGame = true;
            }
            else
            {
                Time.timeScale = 1;
                pauseGame = false;
            }
            
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            print("addTime");
            spendingTime += 100;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            print("quals");
            if (!qualsUp)
            {
                quals.SetActive(true);
                qualsUp = true;
            }
            else
            {
                Time.timeScale = 1;
                quals.SetActive(false);
                qualsUp = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            if (!spawningEnemies)
            {
                spawningEnemies = true;
            }
            else
            {
                Time.timeScale = 1;
                spawningEnemies = false;
            }
        }
    }

    IEnumerator AddSpendingTime()
    {
        while (true)
        {
            spendingTime += spendingTimeIncrease;
            yield return new WaitForSeconds(1f);
        }
    }
    IEnumerator SpawnEnemies()
    {
        yield return new WaitForSeconds(startWave);
        while (spawningEnemies)
        {
            GameObject enemy = Instantiate(enemyPrefab, enemySpawnPos);
            enemyUnits.Add(enemy);
            yield return new WaitForSeconds(Random.Range(waitTimeMin, waitTimeMax));
        }
    }


}
