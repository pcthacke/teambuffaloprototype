﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgeDragon : MonoBehaviour {
    public GameObject eggPrefab;
    public GameObject babyPrefab;
    public GameObject midPrefab;
    public GameObject oldPrefab;
    public GameObject deadPrefab;


    GameManager gameManagerScript;

    Sprite deadDragonSprite;

    //change to private
    public float agingTimer;
    public int timeInterval;
    bool dead = false;

    void Awake()
    {
        timeInterval = 10;
        agingTimer = Time.deltaTime + timeInterval;
    }


    // Use this for initialization
    void Start () {
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        deadDragonSprite = deadPrefab.GetComponent<SpriteRenderer>().sprite;
	}
	
	// Update is called once per frame
	void Update () {
        agingTimer += Time.deltaTime;

        if (agingTimer >= 0 && agingTimer < timeInterval && !this.gameObject.name.Contains(eggPrefab.name))
        {
            print(eggPrefab.name);
            ChangeDragon(eggPrefab, true);
        }
        else if (agingTimer >= timeInterval && agingTimer < timeInterval * 2 && !this.gameObject.name.Contains(babyPrefab.name))
        {
            print(babyPrefab.name);
            ChangeDragon(babyPrefab, true);
        }
        else if (agingTimer >= timeInterval * 2 && agingTimer < timeInterval * 3 && !this.gameObject.name.Contains(midPrefab.name))
        {
            print(midPrefab.name);
            ChangeDragon(midPrefab, true);
        }
        else if (agingTimer >= timeInterval * 3 && agingTimer < timeInterval * 4 && !this.gameObject.name.Contains(oldPrefab.name))
        {
            print(oldPrefab.name);
            ChangeDragon(oldPrefab, true);
        }
        else if (agingTimer >= timeInterval * 4 && !dead)
        {
            dead = true;
            print(deadPrefab.name);
            DeadDragon(deadPrefab);
        }

    }

    void SetAgingTimer(float timer)
    {
        this.agingTimer = timer;
    }

    public void ChangeDragon(GameObject prefabToUse, bool setAgingTimer)
    {
        //switch to other prefab
        GameObject changedDragon = Instantiate(prefabToUse, this.transform.position, this.transform.rotation);
        if (setAgingTimer)
        {
            changedDragon.GetComponent<AgeDragon>().SetAgingTimer(agingTimer);
        }
        else
        {
            float atime = FindProperAgingTime(changedDragon.gameObject.name);
            changedDragon.GetComponent<AgeDragon>().SetAgingTimer(atime);
        }
        if (gameManagerScript.changeCharMenuActive && this.gameObject == gameManagerScript.changingCharacter)
        {
            for (int i = 0; i < gameManagerScript.changeCharButtonList.Count; i++)
            {
                gameManagerScript.changeCharButtonList[i].GetComponent<CharChangeButtonClass>().player = changedDragon;
                gameManagerScript.changingCharacter = changedDragon;
            }
        }
        if (gameManagerScript.doubleTime)
        {
            changedDragon.GetComponent<CharacterClass>()._moveSpeed = changedDragon.GetComponent<CharacterClass>()._moveSpeed / 2;
        }
        int pos = gameManagerScript.playerUnits.IndexOf(this.gameObject);
        print(pos);
        gameManagerScript.playerUnits[pos] = changedDragon;
        Destroy(this.transform.gameObject);
    }
    void DeadDragon(GameObject prefabToUse)
    {
        StartCoroutine(WaitToDieCor());

    }

    IEnumerator WaitToDieCor()
    {
        this.gameObject.GetComponent<SpriteRenderer>().sprite = deadDragonSprite;
        this.gameObject.GetComponent<CharacterClass>()._moveSpeed = 0;
        yield return new WaitForSeconds(3);
        gameManagerScript.playerUnits.Remove(this.gameObject);
        Destroy(this.gameObject);
    }

    float FindProperAgingTime(string dragonName)
    {
        float atime = 0;
        if (dragonName.Contains("EggDragon"))
        {
            return 0;
        }
        else if (dragonName.Contains("BabyDragon"))
        {
            return timeInterval;
        }
        else if (dragonName.Contains("MidDragon"))
        {
            return timeInterval * 2;
        }
        else if (dragonName.Contains("OldDragon"))
        {
            return timeInterval * 3;
        }
        else if (dragonName.Contains("DeadDragon"))
        {
            return timeInterval * 4;
        }
        return atime;
    }

}
