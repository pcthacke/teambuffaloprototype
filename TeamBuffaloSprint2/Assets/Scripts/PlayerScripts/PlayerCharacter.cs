﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : CharacterClass {

	// Use this for initialization
	void Awake () {
        SetCost();
        SetHealth();
        SetDamageRange();
        SetMovementSpeed();
        SetDamageSpeed();
        SetChargeTime();
        SetFullCharge();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void SetCost() {
        _cost = 10;
    }
    public override void SetHealth()
    {
        _maxHealth = 10;
        _currentHealth = _maxHealth;
    }
    public override void SetDamageRange()
    {
        _damageRange = 10;
    }
    public override void SetMovementSpeed()
    {
        _moveSpeed = .5f;
    }
    public override void SetDamageSpeed()
    {
        _damageSpeed = 10;
    }
    public override void SetChargeTime()
    {
        _chargeTime = .1f;
    }
    public override void SetFullCharge()
    {
        _fullCharge = 10;
    }
}
