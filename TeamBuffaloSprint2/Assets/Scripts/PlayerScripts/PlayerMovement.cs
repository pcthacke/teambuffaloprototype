﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public bool canMove = true;
    float moveSpeed;

	// Use this for initialization
	void Start () {
        moveSpeed = this.gameObject.GetComponent<CharacterClass>().GetMovementSpeed();
		
	}
	
	// Update is called once per frame
	void Update () {
        if (canMove)
        {
            moveSpeed = this.gameObject.GetComponent<CharacterClass>().GetMovementSpeed();
            this.transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }
	}



}
