﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdultCharacter : CharacterClass
{

    // Use this for initialization
    void Awake()
    {
        SetCost();
        SetHealth();
        SetDamageRange();
        SetMovementSpeed();
        SetDamageSpeed();
        SetChargeTime();
        SetFullCharge();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void SetCost()
    {
        _cost = 10;
    }
    public override void SetHealth()
    {
        _maxHealth = 40;
        _currentHealth = _maxHealth;
    }
    public override void SetDamageRange()
    {
        _damageRange = 20;
    }
    public override void SetMovementSpeed()
    {
        _moveSpeed = 3f;
    }
    public override void SetDamageSpeed()
    {
        _damageSpeed = 10;
    }
    public override void SetChargeTime()
    {
        _chargeTime = .3f;
    }
    public override void SetFullCharge()
    {
        _fullCharge = 10;
    }
}
