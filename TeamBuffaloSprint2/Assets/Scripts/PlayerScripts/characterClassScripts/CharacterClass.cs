﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterClass : MonoBehaviour {
    public int _cost;
    public int _maxHealth;
    public int _currentHealth;
    public int _damageRange;
    public float _moveSpeed;
    public int _damageSpeed;
    public float _fullCharge;
    public float _chargeTime;

    public float GetMovementSpeed()
    {
        return _moveSpeed;
    }

    public abstract void SetCost();
    public abstract void SetHealth();
    public abstract void SetDamageRange();
    public abstract void SetMovementSpeed();
    public abstract void SetDamageSpeed();
    public abstract void SetFullCharge();
    public abstract void SetChargeTime();
}
