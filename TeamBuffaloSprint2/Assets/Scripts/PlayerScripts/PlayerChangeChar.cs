﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerChangeChar : MonoBehaviour {

    Vector2 pos = new Vector2(20, 40);
    Vector2 size = new Vector2(90, 20);
    public Texture2D emptyBarTexture;
    public Texture2D fullBarTexture;

    public float fullCharge;
    public float currentCharge;
    float progress;

    float timeToCharge;

    float health;

    bool charging = true;
    bool displayChargeMenu = false;
    bool clicked = false;

    Canvas charPickMenuCanvas;
    GameManager gameManagerScript;

    void Start()
    {
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        charPickMenuCanvas = gameManagerScript.characterChangeCanvas;
        //changeCharButton = gameManagerScript.GetComponent<GameManager>().changeCharButton;
        fullCharge = this.gameObject.GetComponent<CharacterClass>()._fullCharge;
        timeToCharge = this.gameObject.GetComponent<CharacterClass>()._chargeTime;
    }

    void OnGUI()
    {
        Vector2 targetPos = Camera.main.WorldToScreenPoint(transform.position);
        GUI.BeginGroup(new Rect(targetPos.x - 30, targetPos.y + 150, size.x, size.y));
        GUI.DrawTexture(new Rect(0, 0, size.x, size.y), emptyBarTexture);

        ////draw the filled-in part:
        GUI.BeginGroup(new Rect(0, 0, size.x * progress, size.y));
        GUI.DrawTexture(new Rect(0,0, size.x, size.y), fullBarTexture);

        GUI.EndGroup();
        GUI.EndGroup();


        if (displayChargeMenu)
        {
            GUI.Box(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 160, 200, 100), "");

            //if the button is pressed means if exists
            if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 - 160, 30, 30), emptyBarTexture))
            {
                print("menuClicked");
            }
        }
    }

    void Update()
    {
        progress = currentCharge / fullCharge;
        if (charging)
        {
            currentCharge += timeToCharge;
            if(currentCharge >= fullCharge)
            {
                StartCoroutine(CharacterChargedCor());
            }
        }
    }

    IEnumerator CharacterChargedCor()
    {
        charging = false;
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;

        //flash sprite to show charged
        yield return 0;

        this.gameObject.GetComponent<SpriteRenderer>().enabled = true;


    }

    void OnMouseDown()
    {
        if (!clicked && !charging)
        {
            clicked = true;
            DisplayCharChangeMenu(true);
        }
        else if(clicked && !charging)
        {
            clicked = false;
            DisplayCharChangeMenu(false);
        }
    }

    void DisplayCharChangeMenu(bool display)
    {
        gameManagerScript.changeCharMenuActive = display;
        charPickMenuCanvas.gameObject.SetActive(display);
        gameManagerScript.changingCharacter = this.gameObject;
        for (int i = 0; i < gameManagerScript.changeCharButtonList.Count; i++)
        {

            gameManagerScript.changeCharButtonList[i].GetComponent<CharChangeButtonClass>().player = this.gameObject;
        }


    }
}
