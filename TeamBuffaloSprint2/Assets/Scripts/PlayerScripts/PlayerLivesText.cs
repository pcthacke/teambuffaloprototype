﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLivesText : MonoBehaviour {

    Text playerLivesText;
    GameManager gameManagerScript;

    // Use this for initialization
    void Start()
    {
        playerLivesText = this.GetComponent<Text>();
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

    }

    // Update is called once per frame
    void Update()
    {
        playerLivesText.text = "Player Lives: " + gameManagerScript.playerLives;

    }
}
