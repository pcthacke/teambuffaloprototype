﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacking : MonoBehaviour {
    GameManager gameManagerScript;
    int damageSpeed;
    int damageRange;

    List<GameObject> enemiesAttacking = new List<GameObject>();

	// Use this for initialization
	void Start () {
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        damageSpeed = this.gameObject.GetComponent<CharacterClass>()._damageSpeed;
        damageRange = this.gameObject.GetComponent<CharacterClass>()._damageRange;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator AttackCor(GameObject enemyObj)
    {
        bool attacking = true;
        while (attacking)
        {
            if(enemiesAttacking == null || enemiesAttacking.Count <= 0 || enemyObj.GetComponent<EnemyHealth>().health <= 0)
            {
                enemiesAttacking.Remove(enemyObj);
                attacking = false;
                if(enemiesAttacking == null || enemiesAttacking.Count <= 0)
                {
                    this.gameObject.GetComponent<PlayerMovement>().canMove = true;
                }
            }
            else
            {
                enemyObj.GetComponent<CharacterClass>()._currentHealth -= damageRange;
                if(enemyObj.GetComponent<CharacterClass>()._currentHealth <= 0)
                {
                    enemiesAttacking.Remove(enemyObj);
                    enemyObj.GetComponent<EnemyHealth>().deadEnemy();
                    attacking = false;
                    if (enemiesAttacking == null || enemiesAttacking.Count <= 0)
                    {
                        this.gameObject.GetComponent<PlayerMovement>().canMove = true;
                    }
                }
            }
            yield return new WaitForSeconds(damageSpeed);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            this.gameObject.GetComponent<PlayerMovement>().canMove = false;
            enemiesAttacking.Add(other.gameObject);
            StartCoroutine(AttackCor(other.gameObject));
        }
    }
}
