﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharChangeButtonClass : MonoBehaviour {
    public Sprite portalSprite;
    public GameObject player;

    Button thisButton;
    public GameObject replacementPrefab;
    public Canvas changeCharCanvas;

    bool canClick = true;

    void Start()
    {
        thisButton = this.gameObject.GetComponent<Button>();
        thisButton.onClick.AddListener(TaskOnClick);
        changeCharCanvas = GameObject.Find("GameManager").GetComponent<GameManager>().characterChangeCanvas;
    }

    void TaskOnClick()
    {
        
        StartCoroutine(CharacterChangeCor());
        

    }

    IEnumerator CharacterChangeCor()
    {
        //TODO: have object player switch if aged too much
        //??? not sure what this means anymore. will keep note in hope of remembering some year
        GameObject parent = this.transform.parent.gameObject;
        Vector3 pos = parent.GetComponent<RectTransform>().position;
        pos.y += 5000f;
        parent.GetComponent<RectTransform>().position = pos;
        //set portal image
        print("change");
        player.GetComponent<SpriteRenderer>().sprite = portalSprite;
        //disable movement
        player.GetComponent<PlayerMovement>().enabled = false;

        yield return new WaitForSeconds(5f);
        print("waited");

        //switch to other prefab
        Instantiate(replacementPrefab, player.transform.position, player.transform.rotation);
        Destroy(player);
        pos = parent.GetComponent<RectTransform>().position;
        pos.y -= 5000f;
        parent.GetComponent<RectTransform>().position = pos;

        changeCharCanvas.gameObject.SetActive(false);

    }
}
