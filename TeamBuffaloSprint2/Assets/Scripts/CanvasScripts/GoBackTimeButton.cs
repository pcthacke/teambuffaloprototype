﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoBackTimeButton : MonoBehaviour
{
    Button thisButton;
    GameManager gameManagerScript;

    public GameObject eggPrefab;
    public GameObject babyPrefab;
    public GameObject midPrefab;
    public GameObject oldPrefab;

    public List<GameObject> newPlayerList = new List<GameObject>();

    int revertTimeCost = 50;

    // Use this for initialization
    void Start()
    {
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        thisButton = this.gameObject.GetComponent<Button>();
        thisButton.onClick.AddListener(TaskOnClick);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void TaskOnClick()
    {
        if (gameManagerScript.spendingTime >= revertTimeCost)
        {
            gameManagerScript.spendingTime -= revertTimeCost;
            for (int i = 0; i < gameManagerScript.playerUnits.Count; i++)
            {
                if (gameManagerScript.playerUnits[i] != null)
                {
                    GameObject prefabSwitchingTo = DragonRevertTo(gameManagerScript.playerUnits[i]);
                    gameManagerScript.playerUnits[i].GetComponent<AgeDragon>().ChangeDragon(prefabSwitchingTo, false);
                }
            }
        }

    }

    GameObject DragonRevertTo(GameObject changingDragon)
    {
        if(changingDragon.name.Contains("BabyDragon") || changingDragon.name.Contains("EggDragon")){
            print("egg");
            return eggPrefab;
        }
        else if (changingDragon.name.Contains("MidDragon"))
        {
            return babyPrefab;
        }
        else if (changingDragon.name.Contains("OldDragon"))
        {
            return midPrefab;
        }
        else
        {
            Debug.LogError("given game object is not within preset range: defaulted to babyPrefab");
            return babyPrefab;
        }
    }
}
