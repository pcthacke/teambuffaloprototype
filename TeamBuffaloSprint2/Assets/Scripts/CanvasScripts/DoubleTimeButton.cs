﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoubleTimeButton : MonoBehaviour
{
    Button thisButton;

    int cost = 20;
    GameManager gameManagerScript;
    float timeWarpSecs = 20;

    // Use this for initialization
    void Start()
    {
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        thisButton = this.gameObject.GetComponent<Button>();
        thisButton.onClick.AddListener(TaskOnClick);

    }

    // Update is called once per frame
    void Update()
    {

    }

    void TaskOnClick()
    {
        if (gameManagerScript.spendingTime >= cost)
        {
            StartCoroutine(TimeWarpForwardCor());
        }
    }

    IEnumerator TimeWarpForwardCor()
    {
        gameManagerScript.doubleTime = true;
        for(int i = 0; i < gameManagerScript.playerUnits.Count; i++)
        {
            float speed = gameManagerScript.playerUnits[i].GetComponent<CharacterClass>().GetMovementSpeed() / 2;
            gameManagerScript.playerUnits[i].GetComponent<CharacterClass>()._moveSpeed = speed;
        }
        Time.timeScale = 2;
        yield return new WaitForSeconds(timeWarpSecs);

        for (int i = 0; i < gameManagerScript.playerUnits.Count; i++)
        {
            float speed = gameManagerScript.playerUnits[i].GetComponent<CharacterClass>().GetMovementSpeed() * 2;
            gameManagerScript.playerUnits[i].GetComponent<CharacterClass>()._moveSpeed = speed;
        }
        gameManagerScript.doubleTime = false;
        Time.timeScale = 1;
    }
}
