﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLivesText : MonoBehaviour {

    Text enemyLivesText;
    GameManager gameManagerScript;

    // Use this for initialization
    void Start()
    {
        enemyLivesText = this.GetComponent<Text>();
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

    }

    // Update is called once per frame
    void Update()
    {
        enemyLivesText.text = "Enemy Lives: " + gameManagerScript.enemyLives;

    }
}
