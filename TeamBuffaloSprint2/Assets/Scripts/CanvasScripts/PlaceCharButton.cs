﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceCharButton : MonoBehaviour
{
    GameManager gameManagerScript;
    int charCost = 30;

    public Transform playerSpawnPoint;
    public GameObject babyDragonPrefab;

    Button thisButton;

    // Use this for initialization
    void Start()
    {
        thisButton = this.gameObject.GetComponent<Button>();
        thisButton.onClick.AddListener(TaskOnClick);
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    void TaskOnClick()
    {
        if (gameManagerScript.spendingTime >= charCost)
        {
            print("spawn");
            StartCoroutine(SpawnCharCor());
        }


    }

    IEnumerator SpawnCharCor()
    {
        gameManagerScript.spendingTime -= charCost;
        GameObject newChar = Instantiate(babyDragonPrefab, playerSpawnPoint);
        gameManagerScript.playerUnits.Add(newChar);
        if (gameManagerScript.doubleTime)
        {
            newChar.GetComponent<CharacterClass>()._moveSpeed = newChar.GetComponent<CharacterClass>()._moveSpeed / 2;
        }


        yield return 0;
    }
}
