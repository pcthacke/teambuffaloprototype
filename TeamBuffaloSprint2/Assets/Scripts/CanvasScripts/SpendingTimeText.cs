﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpendingTimeText : MonoBehaviour {

    public GameObject spendingTextObj;
    Text spendingText;
    public GameObject gameManager;
    GameManager gameManagerScript;

	// Use this for initialization
	void Start () {
        spendingText = spendingTextObj.GetComponent<Text>();
        gameManagerScript = gameManager.GetComponent<GameManager>();
		
	}
	
	// Update is called once per frame
	void Update () {
        spendingText.text = "Time: " + gameManagerScript.spendingTime;
		
	}
}
