﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public int health = 20;

    GameManager gameManagerScript;

	// Use this for initialization
	void Start () {
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void deadEnemy()
    {
        gameManagerScript.enemyUnits.Remove(this.gameObject);
        Destroy(this.gameObject);
    }
}
