﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public bool canMove = true;
    float moveSpeed;

    //GameManager gameManagerScript;

    // Use this for initialization
    void Start()
    {
        //gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        moveSpeed = this.gameObject.GetComponent<CharacterClass>()._moveSpeed;

    }

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            this.transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }
    }
}
