﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttacking : MonoBehaviour {

    GameManager gameManagerScript;
    int damageSpeed;
    int damageRange;

    public List<GameObject> playersAttacking = new List<GameObject>();

	// Use this for initialization
	void Start () {
        gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        damageSpeed = this.gameObject.GetComponent<CharacterClass>()._damageSpeed;
        damageRange = this.gameObject.GetComponent<CharacterClass>()._damageRange;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator AttackCor(GameObject playerObj)
    {
        bool attacking = true;
        while (attacking)
        {
            if(playersAttacking == null || playersAttacking.Count <= 0 || playerObj.GetComponent<EnemyHealth>().health <= 0)
            {
                playersAttacking.Remove(playerObj);
                attacking = false;
                if(playersAttacking == null)
                {
                    this.gameObject.GetComponent<EnemyMovement>().canMove = true;
                }
            }
            else
            {
                playerObj.GetComponent<CharacterClass>()._currentHealth -= damageRange;
                if(playerObj.GetComponent<CharacterClass>()._currentHealth <= 0)
                {
                    playersAttacking.Remove(playerObj);
                    gameManagerScript.playerUnits.Remove(playerObj);
                    Destroy(playerObj);
                    attacking = false;
                    if (playersAttacking == null || playersAttacking.Count <= 0)
                    {
                        this.gameObject.GetComponent<EnemyMovement>().canMove = true;
                    }
                }
            }
            yield return new WaitForSeconds(damageSpeed);
        }
    }
    

    void OnTriggerEnter2D(Collider2D other)
    {
        print("colliding");
        if (other.gameObject.tag == "Player")
        {
            print("found");
            this.gameObject.GetComponent<EnemyMovement>().canMove = false;
            playersAttacking.Add(other.gameObject);
            StartCoroutine(AttackCor(other.gameObject));
        }
    }

}
